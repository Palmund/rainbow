load 'rainbow-core.rb'

hashes = Core::fetch_chains()
i = 0
hashes.each_pair do |plain_text, hash|
  puts "#{i}) #{plain_text} -> #{hash}"
  i += 1
end

puts 'Select chain: '
chain = gets.chomp.to_i
plain_text = hashes.keys[chain]
end_of_chain = hashes[plain_text]

hashed_text = Core::hash(plain_text)
while hashed_text != end_of_chain do
  puts "#{plain_text} -> #{hashed_text}"
  plain_text = Core::reduce(hashed_text)
  hashed_text = Core::hash(plain_text)
end
puts "#{plain_text} -> #{hashed_text}"