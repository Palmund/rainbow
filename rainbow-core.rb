require 'digest'
require 'base64'

class Core
  @path = File.expand_path(File.dirname(__FILE__))

  def self.hash(plain_text)
    Digest::MD5.hexdigest plain_text
  end

  def self.reduce(hash, length = 10)
    Base64.encode64(hash)[0, length]
  end

  def self.reduce_hash(hash, length = 10)
    return hash(reduce(hash, length))
  end

  def self.fetch_chains(file = "#{@path}/hash")
    hashes = {}
    text = File.open(file, 'r').read
    text.each_line do |line|
      chunks = line.delete("\n").split(' -> ')
      hashes[chunks[0]] = chunks[1]
    end
    return hashes
  end

  def self.store_chain(chain)
    chains = fetch_chains()
    chains[chain[:key]] = chain[:hash]
    File.open("#{@path}/hash", 'w') { |file|
      chains.each_pair { |key, hash|
        file.write "#{key} -> #{hash}\n"
      }
    }
  end
end