path = File.expand_path(File.dirname(__FILE__))

load "#{path}/rainbow-core.rb"

def generate_chain(plain_text, links)
  hashed_text = Core::hash(plain_text)
  return hashed_text if links == 1
  1.upto(links) {
    hashed_text = Core::reduce_hash(hashed_text, 10)
  }
  hashed_text
end

if ARGV.empty?
  puts 'Usage: generate [plain_text] [number_of_links]'
else
  plain_text = ARGV[0]
  number_of_links = ARGV[1].to_i
  hashed_text = generate_chain(plain_text, number_of_links)
  puts "#{plain_text} -> #{hashed_text}"
  Core::store_chain({
                        :key => plain_text,
                        :hash => hashed_text
                    })
end