path = File.expand_path(File.dirname(__FILE__))

load "#{path}/rainbow-core.rb"

@chains = Core::fetch_chains()

def find_chain(hash_needle, attempts = 100)
  hashes = @chains.values
  (0..attempts).each do
    if hashes.include? hash_needle
      return {:key => @chains.key(hash_needle), :hash => hash_needle}
    end
    reduce = Core::reduce(hash_needle)
    hash_needle = Core::hash(reduce)
  end
  nil
end

@hash = '961a32bf06338872cb08fec5af40b300'

if ARGV.empty?
  puts 'Usage: [hash] ( [attempts] )'
  exit()
else
  @hash = ARGV[0]
  @attempts = ARGV.count == 2 ? ARGV[1] : 100
end

chain = find_chain(@hash, @attempts)
hashed_text = Core::hash(chain[:key])
plain_text = chain[:key]
while hashed_text != chain[:hash]
  if hashed_text == @hash
    puts plain_text
    break
  end
  plain_text = Core::reduce(hashed_text)
  hashed_text = Core::hash(plain_text)
end

puts plain_text == 'Njg3MTI1YW'