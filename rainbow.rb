if ARGV.empty?
  puts 'Usage: [generate|solve]'
  exit()
end

path = File.expand_path(File.dirname(__FILE__))
command = ARGV.delete(ARGV[0])

print `ruby #{path}/rainbow-#{command}.rb #{ARGV.join(' ')}`